--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:09:49 10/31/2018
-- Design Name:   
-- Module Name:   testUART.vhd
-- Project Name:  uart
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: TxUnit
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

ENTITY testUART IS
  END testUART;

ARCHITECTURE behavior OF testUART IS 

  -- Component Declaration for the Unit Under Test (UUT)
  component UART_FPGA_N4 is
    PORT (
           btnC : IN STD_LOGIC;
           led : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
           TxD : OUT STD_LOGIC;
           RxD : IN STD_LOGIC;
           mclk : IN STD_LOGIC
         );
  END component;



  signal clk : std_logic := '0';
  signal btnC : std_logic := '0';

  -- Clock period definitions
  constant clk_period : time := 10 ns;
  constant rxd_period : time := 208333 ns; -- pour 9600 bauds (on fait * 2 à la durée d’un bit, ie demi-période)

  signal txd, rxd : STD_LOGIC;
  signal led : std_logic_vector(15 downto 0);

BEGIN

  -- Unit under test
  uut : UART_FPGA_N4
  PORT MAP (
             btnC => btnC,
             led => led,
             TxD => txd,
             RxD => rxd,
             mclk => clk
           );

  -- Clock process definitions
  clk_process :process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;

  -- Clock process definitions
  -- on utilise une clock comme rxd pour tester
  rxd_process :process
  begin
    rxd <= '0';
    wait for rxd_period/2;
    rxd <= '1';
    wait for rxd_period/2;
  end process;

  -- Stimulus process
  stim_proc: process
  begin		
    -- clic sur le bouton
    wait for 10 ns;	
    btnC <= '1';
    wait for 100 ns;	
    btnC <= '0';

    wait;

  end process;

END;
