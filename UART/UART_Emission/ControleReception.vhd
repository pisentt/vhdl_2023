LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

entity ControleReception is
  port (
         clk : in std_logic;    -- horloge de base, rapide
         reset: in std_logic;   -- remise � zero
         rd : in std_logic;     -- lecture par le processeur
         tmpclk : in std_logic; -- clock g�n�r�e par le Compteur16
         tmprxd : in std_logic; -- recopie de rxd synchronis�e avec tmpclk
         Ferr : out std_logic;  -- bit de stop erron� ?
         OErr : out std_logic;  -- donn�e pas lu � temps par le processeur ?
         DRdy : out std_logic;  -- donn�e dispo, � lire par le processeur
         data : out std_logic_vector(7 downto 0) -- la donn�e re�ue par la liaison s�rie
       );
end entity ControleReception;

architecture behavorial of ControleReception is

  -- idle: attente de r�ceiption de bit start
  -- read_data: lecture de tmprxd
  -- read_proc: envoyer la data au processeur et lui laisser le temps de la lire
  type state is (idle, read_data, read_proc);

begin
  automate : process(clk, reset)
    variable cpt : integer range 0 to 8 := 0; -- compteur pour savoir � quel bit on en est
    variable etat : state := idle; -- �tat actuel de l'automate
  begin
    if reset = '0' then
      Ferr <= '0';
      OErr <= '0';
      DRdy <= '0';
      data <= (others => '0');
    elsif rising_edge(clk) then
      case etat is
        when idle =>
          Ferr <= '0';
          OErr <= '0';
          DRdy <= '0';
          if tmpclk = '1' and tmprxd = '0' then
            cpt := 8;
            etat := read_data;
          end if;
        when read_data =>
          if tmpclk = '1' then
            if cpt > 0 then
              cpt := cpt - 1;
              data(cpt) <= tmprxd;
            elsif tmprxd = '0' then -- mauvais bit de stop
              Ferr <= '1';
              etat := idle;
            else -- bon bit de stop
              DRdy <= '1';
              etat := read_proc;
            end if;
          end if;
        when read_proc =>
          DRdy <= '0';
          if rd = '0' then -- le processeur n'a pas lu la donn�e
            OErr <= '1';
          end if;
          etat := idle;
      end case;
    end if;
  end process automate ;
end architecture behavorial;
