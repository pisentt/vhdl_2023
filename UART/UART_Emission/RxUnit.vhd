LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY RxUnit IS
  PORT (
  clk, reset : IN STD_LOGIC;
  enable : IN STD_LOGIC;
  read : IN STD_LOGIC;
  rxd : IN STD_LOGIC;
  data : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
  Ferr, OErr, DRdy : OUT STD_LOGIC);
END RxUnit;

ARCHITECTURE behavorial OF RxUnit IS

  component Compteur16 is
    port (
           enable : in std_logic; -- horloge du compteur
           reset : in std_logic; -- remise � z�ro 
           rxd : in std_logic; -- data en entr�e (� lire)
           tmpclk : out std_logic; -- horloge ralentie et d�phas�e de moiti� (pour lire rxd)
           tmprxd : out std_logic -- rxd apr�s lecture
         );
  end component;

  component ControleReception is
    port (
           clk : in std_logic; -- horloge
           reset: in std_logic; -- remise � z�ro
           rd : in std_logic; -- vaut 1 si on souhaite lire (read)
           tmpclk : in std_logic; -- sert � connecter l'horloge au Compteur16
           tmprxd : in std_logic; -- sert � connecter le signal des donn�es re�ues au Compteur16
           Ferr : out std_logic; -- vaut 1 si la trame re�ue est erron�e
           OErr : out std_logic; -- vaut 1 si une donn�e n'a pas �t� lue
           DRdy : out std_logic; -- vaut 1 si une donn�e est re�ue
           data : out std_logic_vector(7 downto 0) -- donn�es re�ues
         );
  end component;

   -- signaux pour connecter les composants
  signal tmpclk_sig, tmprxd_sig : std_logic;

 begin

   compteur_16 : Compteur16
   port map (
              enable => enable,
              reset => reset,
              rxd => rxd,
              tmpclk => tmpclk_sig,
              tmprxd => tmprxd_sig
            );

   controle_reception : ControleReception
   port map (
              clk => clk,
              reset => reset,
              rd => read,
              tmpclk => tmpclk_sig,
              tmprxd => tmprxd_sig,
              Ferr => Ferr,
              OErr => OErr,
              DRdy => DRdy,
              data => data
            );

end behavorial;
