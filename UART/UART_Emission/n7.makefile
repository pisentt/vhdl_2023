
SRC = ../ClkUnit/clkUnit.vhd \
      ../TxUnit/TxUnit.vhd   \
			UART.vhd               \
			RxUnit.vhd             \
			ctrlUnit.vhd           \
			echoUnit.vhd           \
			testUART.vhd           \
			diviseurClk.vhd        \
			Compteur16.vhd         \
			ControleReception.vhd  \
			UART_FPGA_N4.vhd

# for simulation:
TEST = testTxUnit
# duration (to adjust if necessary)
TIME = 3000us
PLOT = output

# for synthesis:
UNIT = UART_FPGA_N4
ARCH = synthesis
UCF  = UART_FPGA_N4.ucf
