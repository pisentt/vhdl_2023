LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY UART_FPGA_N4 IS
  PORT (
    -- ne garder que les ports utiles ?
    -- les 16 switchs
    btnC : IN STD_LOGIC;
    -- les 5 boutons noirs
    -- btnC, btnU, btnL, btnR, btnD : IN STD_LOGIC;
    -- horloge
    mclk : IN STD_LOGIC;
    -- les 16 leds
    led : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
    -- les anodes pour sélectionner les afficheurs 7 segments à utiliser
    -- an : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
    -- valeur affichée sur les 7 segments (point décimal compris, segment 7)
    -- ssg : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
    -- ligne série (à rajouter)
    TxD : OUT STD_LOGIC;
    RxD : IN STD_LOGIC
  );
END UART_FPGA_N4;

ARCHITECTURE synthesis OF UART_FPGA_N4 IS

  -- rappel du (des) composant(s)
  COMPONENT UARTunit
    PORT (
      clk, reset : IN STD_LOGIC;
      cs, rd, wr : IN STD_LOGIC;
      RxD : IN STD_LOGIC;
      TxD : OUT STD_LOGIC;
      IntR, IntT : OUT STD_LOGIC;
      addr : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
      data_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      data_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
    );
  END COMPONENT;

  COMPONENT echoUnit IS
    PORT (
      clk, reset : IN STD_LOGIC; -- horloge et ràz
      cs, rd, wr : OUT STD_LOGIC; -- flags si l'on souhaite lire (rd=read) ou écrire (wr=write), et cs vaut toujours 0
      IntR : IN STD_LOGIC; -- interruption de réception
      IntT : IN STD_LOGIC; -- interruption d'émission
      addr : OUT STD_LOGIC_VECTOR(1 DOWNTO 0); -- sélectionne si data_out contient le registre de contrôle (avec addr=01) (cf ctrlUnit) ou les données reçues (avec addr=00)
      data_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0); -- données en entrée
      data_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)); -- données en sortie
  END COMPONENT;

  COMPONENT diviseurClk
    GENERIC(facteur: natural);
    PORT(
          clk : IN  std_logic; -- horloge de base
          reset : IN  std_logic; -- remise à zéro
          nclk : OUT  std_logic -- horloge divisée par facteur
        );
  END COMPONENT;

  -- données reçues (en sortie de UARTUnit et en entrée de echoUnit)
  SIGNAL sig_uart_in : STD_LOGIC_VECTOR(7 DOWNTO 0);
  -- données émises (en entrée de UARTUnit et en sortie de echoUnit)
  SIGNAL sig_uart_out : STD_LOGIC_VECTOR(7 DOWNTO 0);
  -- les noms des signaux correspondent exactement (à un préfixe sig_ près) à des ports sur echoUnit ou UARTUnit
  SIGNAL sig_addr : STD_LOGIC_VECTOR(1 DOWNTO 0);
  SIGNAL sig_cs, sig_rd, sig_wr : STD_LOGIC;
  SIGNAL sig_IntR, sig_IntT : STD_LOGIC;
  SIGNAL nclk : STD_LOGIC;
  SIGNAL reset : STD_LOGIC;
BEGIN
  -- convention afficheur 7 segments 0 => allumé, 1 => éteint
  -- ssg <= (OTHERS => '1');
  -- aucun afficheur sélectionné
  -- an(7 DOWNTO 0) <= (OTHERS => '1');
  -- 16 leds éteintes
  -- led(14 DOWNTO 8) <= swt(14 DOWNTO 8);

  led(7 DOWNTO 0) <= sig_uart_out;

  led(15 DOWNTO 8) <= sig_uart_in;

  reset <= not(btnC);

   inst_diviseurClk : diviseurClk
   GENERIC MAP(651)
   -- GENERIC MAP(2)
   PORT MAP(
          clk => mclk,
          reset => reset,
          nclk => nclk
        );

  -- connexion du (des) composant(s) avec les ports de la carte
  inst_uartUnit : UARTunit PORT MAP(
                                     addr => sig_addr,
                                     clk => nclk,
                                     cs => sig_cs,
                                     data_in => sig_uart_in,
                                     data_out => sig_uart_out,
                                     IntR => sig_IntR,
                                     IntT => sig_IntT,
                                     rd => sig_rd,
                                     wr => sig_wr,
                                     reset => reset,
                                     RxD => RxD,
                                     TxD => TxD
                                   );

  inst_echoUnit : echoUnit PORT MAP(
                                     clk => nclk,
                                     cs => sig_cs,
                                     data_in => sig_uart_out,
                                     data_out => sig_uart_in,
                                     IntR => sig_IntR,
                                     IntT => sig_IntT,
                                     rd => sig_rd,
                                     wr => sig_wr,
                                     reset => reset,
                                     addr => sig_addr
                                   );

END synthesis;
