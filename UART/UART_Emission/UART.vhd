LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY UARTunit IS
  PORT (
  clk, reset : IN STD_LOGIC; -- horloge et remise � z�ro
  cs, rd, wr : IN STD_LOGIC; -- flags: rd = 1 si on veut lire, wr = 1 si on veut �crire, et cs toujours � 0
  RxD : IN STD_LOGIC; -- signal d'entr�e des donn�es, provenant de RxUnit
  TxD : OUT STD_LOGIC; -- signal de sortie des donn�es, allant vers TxUnit
  IntR, IntT : OUT STD_LOGIC; -- interruptions: R = r�ception, T = transmission
  addr : IN STD_LOGIC_VECTOR(1 DOWNTO 0); -- addresse de s�lection de la sortie: 00 pour les donn�es, 01 pour le registre de contr�le (cf ctrlUnit)
  data_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0); -- donn�es en entr�e
  data_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)); -- donn�es en sortie
END UARTunit;

ARCHITECTURE UARTunit_arch OF UARTunit IS

  COMPONENT clkUnit IS
    PORT (
    clk, reset : IN STD_LOGIC;
    enableTX : OUT STD_LOGIC;
    enableRX : OUT STD_LOGIC);
  END COMPONENT;

  COMPONENT TxUnit IS
    PORT (
    clk, reset : IN STD_LOGIC;
    enable : IN STD_LOGIC;
    ld : IN STD_LOGIC;
    txd : OUT STD_LOGIC;
    regE : OUT STD_LOGIC;
    bufE : OUT STD_LOGIC;
    data : IN STD_LOGIC_VECTOR(7 DOWNTO 0));
  END COMPONENT;

  COMPONENT RxUnit IS
    PORT (
    clk, reset : IN STD_LOGIC;
    enable : IN STD_LOGIC;
    read : IN STD_LOGIC;
    rxd : IN STD_LOGIC;
    data : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    Ferr, OErr, DRdy : OUT STD_LOGIC);
  END COMPONENT;

  COMPONENT ctrlUnit IS
    PORT (
    clk, reset : IN STD_LOGIC;
    rd, cs : IN STD_LOGIC;
    DRdy, FErr, OErr : IN STD_LOGIC;
    BufE, RegE : IN STD_LOGIC;
    IntR : OUT STD_LOGIC;
    IntT : OUT STD_LOGIC;
    ctrlReg : OUT STD_LOGIC_VECTOR(7 DOWNTO 0));

  END COMPONENT;

  SIGNAL lecture, ecriture : STD_LOGIC;
  SIGNAL donnees_recues : STD_LOGIC_VECTOR(7 DOWNTO 0);
  SIGNAL registre_controle : STD_LOGIC_VECTOR(7 DOWNTO 0);

  SIGNAL enableTX, enableRX : STD_LOGIC;
  SIGNAL txBufferEmpty : STD_LOGIC;
  SIGNAL txRegisterEmpty : STD_LOGIC;
  SIGNAL DRdy, FErr, OErr : STD_LOGIC;

BEGIN -- UARTunit_arch

  inst_clkUnit : clkUnit PORT MAP(
                                   clk => clk,
                                   reset => reset,
                                   enableTX => enableTX,
                                   enableRX => enableRX
                                 );

  inst_txunit : TxUnit PORT MAP(
                                 clk => clk,
                                 enable => enableTX,
                                 data => data_in,
                                 ld => ecriture,
                                 txd => TxD,
                                 reset => reset, 
                                 bufE => txBufferEmpty,
                                 regE => txRegisterEmpty
                               );

  inst_rxunit : RxUnit PORT MAP(
                                 clk => clk,
                                 enable => enableRX,
                                 read => lecture,
                                 rxd => RxD,
                                 data => donnees_recues,
                                 Ferr => Ferr,
                                 DRdy => DRdy,
                                 OErr => OErr,
                                 reset => reset 
                               );

  inst_ctrlunit : ctrlUnit PORT MAP(
                                     clk => clk,
                                     reset => reset, 
                                     BufE => txBufferEmpty,
                                     DRdy => DRdy,
                                     FErr => FErr,
                                     OErr => OErr,
                                     RegE => txRegisterEmpty,
                                     rd => rd,
                                     cs => cs,
                                     IntR => IntR,
                                     IntT => IntT,
                                     ctrlReg => registre_controle
                                   );

  lecture <= '1' WHEN cs = '0' AND rd = '0' ELSE
             '0';
  ecriture <= '1' WHEN cs = '0' AND wr = '0' ELSE
              '0';
  data_out <= donnees_recues WHEN lecture = '1' AND addr = "00"
              ELSE
                registre_controle WHEN lecture = '1' AND addr = "01"
              ELSE
                "00000000";

-- a completer par la connexion des differents composants

              END UARTunit_arch;
