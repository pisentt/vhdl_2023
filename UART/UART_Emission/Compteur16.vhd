LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Compteur16 IS
  PORT (
         enable : IN STD_LOGIC; -- horloge du compteur
         reset : IN STD_LOGIC; -- remise � z�ro 
         rxd : IN STD_LOGIC; -- data en entr�e (� lire)
         tmpclk : OUT STD_LOGIC; -- horloge ralentie et d�phas�e de moiti� (pour lire rxd)
         tmprxd : OUT STD_LOGIC -- rxd apr�s lecture
       );
END Compteur16;

ARCHITECTURE behavorial OF Compteur16 IS

   -- idle: attente de r�ceiption de bit start
   -- comptage: compter jusqu'� 16 ticks de enable
   -- pulse: envoyer un pulse sur tmpclk et lire rxd sur tmprxd
  TYPE state IS (idle, comptage, pulse);

BEGIN
  PROCESS (enable, reset) IS
    VARIABLE nb_bits : INTEGER RANGE 0 TO 10 := 0; -- nombre de bits restants � lire
    VARIABLE cpt : INTEGER RANGE 0 TO 15 := 0; -- compteur pour diviser l'horloge par 16
    VARIABLE etat : state := idle; -- �tat actuel de l'automate
  BEGIN
    IF reset = '0' THEN
      etat := idle;
      tmpclk <= '0';
      tmprxd <= '0';
    ELSIF rising_edge(enable) THEN
      CASE etat IS
        WHEN idle =>
          cpt := 6; -- pour d�phaser
          nb_bits := 10; -- le message fait 8 bits + start + stop
          tmpclk <= '0'; -- 
          tmprxd <= '0';
          if rxd = '0' then -- si bit de start
            etat := comptage;
          end if;
        WHEN comptage =>
          IF cpt = 0 THEN
            cpt := 15;
            nb_bits := nb_bits - 1;
            tmpclk <= '1';
            tmprxd <= rxd;
            etat := pulse;
          ELSE
            cpt := cpt - 1;
          END IF;
        WHEN pulse =>
          tmpclk <= '0';
          IF nb_bits = 0 THEN
            etat := idle;
          ELSE
            cpt := cpt - 1;
            etat := comptage;
          END IF;
      END CASE;
    END IF;
  END PROCESS;
END behavorial;
