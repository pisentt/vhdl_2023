#!/usr/bin/env fish 
set ghdl_flags "-fsynopsys --std=08"

for source_file in *.vhd
    set module_name (basename $source_file .vhd)
    echo +a $source_file
    ghdl -a -fsynopsys --std=08 $source_file
    echo +e $module_name
    ghdl -e -fsynopsys --std=08 $module_name
end

for source_file in *.vhd
    set module_name (basename $source_file .vhd)
    ghdl -r -fsynopsys --std=08 $module_name --stop-time=10us --vcd=$module_name.vcd
end
