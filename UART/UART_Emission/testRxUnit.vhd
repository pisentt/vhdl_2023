--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:09:49 10/31/2018
-- Design Name:   
-- Module Name:   testTxUnit.vhd
-- Project Name:  uart
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: TxUnit
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

ENTITY testRxUnit IS
  END testRxUnit;

ARCHITECTURE behavior OF testRxUnit IS 

    -- Component Declaration for the Unit Under Test (UUT)

  component RxUnit IS
    PORT (
    clk, reset : IN STD_LOGIC;
    enable : IN STD_LOGIC;
    read : IN STD_LOGIC;
    rxd : IN STD_LOGIC;
    data : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    Ferr, OErr, DRdy : OUT STD_LOGIC);
  END component;

  COMPONENT clkUnit
    PORT(
          clk : IN  std_logic;
          reset : IN  std_logic;
          enableTX : OUT  std_logic;
          enableRX : OUT  std_logic
        );
  END COMPONENT;	

    -- Vu qu'on a deja testé le TxUnit, autant l'utiliser
    -- pour généréer nos signaux de test
  COMPONENT TxUnit
    PORT(
          clk : IN  std_logic;
          reset : IN  std_logic;
          enable : IN  std_logic;
          ld : IN  std_logic;
          txd : OUT  std_logic;
          regE : OUT  std_logic;
          bufE : OUT  std_logic;
          data : IN  std_logic_vector(7 downto 0)
        );
  END COMPONENT;



  signal clk : std_logic := '0';
  signal reset : std_logic := '0';
  signal enableTx : std_logic := '0';
  signal enableRx : std_logic := '0';

    -- Clock period definitions
  constant clk_period : time := 10 ns;

    -- signaux à tester (vérifier qu'on a les outputs attendues)
  signal Ferr, OErr, DRdy : STD_LOGIC;
  signal data : std_logic_vector(7 downto 0);

    -- signaux générés par le test à envoyer au composant
    -- à tester
  signal read : std_logic := '0';
  signal rxd, ld, txd : std_logic;
  signal data_in : std_logic_vector(7 downto 0);
  signal selection : std_logic; -- permet de choisir si rxd = txd
                                -- ou si on choisit sa valeur manuellement

BEGIN

  -- Instantiate the Unit Under Test
  uut0: RxUnit PORT MAP (
                          clk => clk,
                          reset => reset,
                          enable => enableRX,
                          read => read,
                          rxd => rxd,
                          data => data,
                          Ferr => Ferr,
                          OErr => OErr,
                          DRdy => DRdy);


  -- Instantiate the clkUnit
  clkUnit1: clkUnit PORT MAP (
                               clk => clk,
                               reset => reset,
                               enableTX => enableTX,
                               enableRX => enableRX
                             );

  -- Clock process definitions
  clk_process :process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;


  -- On utilise le TxUnit déjà testé pour nous aider
  -- à générer le signal rxd
  txUnit1: TxUnit port map (
                             clk => clk,
                             enable => enableTx,
                             reset => reset,
                             ld => ld,
                             txd => txd,
                             regE => open,
                             bufE => open,
                             data => data_in
                           );

  -- choisir si rxd est piloté par le TxUnit ou géré manuellement
  rxd <= txd when selection = '1' else '0';

  -- Automate qui sert à générer le test
  stim_proc: process
  begin		
    selection <= '1';
    -- maintien du reset durant 100 ns.
    wait for 100 ns;	
    reset <= '1';
    wait for 100 ns;

    -- TEST TRANSMISSION CORRECTE 1
    data_in <= "10100101"; -- valeur aléatoire
    ld <= '1';
    wait for (clk_period); -- on attend que ld soit vu
    ld <= '0';
    wait for (clk_period * 5); -- on attend le vrai début de l'envoi
    wait for (clk_period * 16 * 9); -- on attend fin envoi (8 bits + start)
    wait for (clk_period * 8); -- le RxUnit travaille avec tmpclk décalée de 8 clocks
    wait for (clk_period); -- on attend que DRdy soit mis à jour
    wait for (clk_period / 2); -- on se désynchronise de l'horloge du RxUnit

    -- tester que la fin d'envoi est correcte
    assert DRdy = '1' report "donnée pas prête à être lue à temps" severity error;
    assert Ferr = '0' report "la donnée n'a pas été correctement reçue" severity error;
    assert data = "10100101" report "c'est pas la donnée qu'on a envoyée !!" severity error;
    read <= '1'; -- on prévient qu'on a lu
    wait for (clk_period); -- on attend que read soit vu
    assert OErr = '0' report "OErr alors que le processeur a lu la data" severity error;
    read <= '0';

    wait for (clk_period / 2); -- on se resynchronise de l'horloge du RxUnit
    wait for 100 ns;

    -- TEST TRANSMISSION CORRECTE 2
    -- envoi de 2 données à la suite
    data_in <= "10110111"; -- valeur aléatoire
    ld <= '1';
    wait for (clk_period); -- on attend que ld soit vu
    ld <= '0';
    data_in <= "11110000"; -- valeur aléatoire numéro 2 envoyée en buffer
    wait for (clk_period); -- on attend que ld soit vu
    ld <= '1'; -- deuxième ordre d'envoi
    wait for (clk_period); -- on attend que ld soit vu
    ld <= '0';
    wait for (clk_period * 8); -- on attend le vrai début de l'envoi
    wait for (clk_period * 16 * 9); -- on attend fin envoi (8 bits + start)
    wait for (clk_period * 8); -- le RxUnit travaille avec tmpclk décalée de 8 clocks
    wait for (clk_period); -- on attend que DRdy soit mis à jour
    wait for (clk_period / 2); -- on se désynchronise de l'horloge du RxUnit

    -- tester que la fin d'envoi est correcte
    assert DRdy = '1' report "donnée pas prête à être lue à temps" severity error;
    assert Ferr = '0' report "la donnée n'a pas été correctement reçue" severity error;
    assert data = "10110111" report "c'est pas la donnée qu'on a envoyée !!" severity error;
    read <= '1'; -- on prévient qu'on a lu
    wait for (clk_period); -- on attend que read soit vu
    assert OErr = '0' report "OErr alors que le processeur a lu la data" severity error;
    read <= '0';
    wait for (clk_period / 2); -- on se resynchronise de l'horloge du RxUnit


    wait for (clk_period * 5); -- on attend le vrai début du second envoi (sans pause)
    wait for (clk_period * 16 * 9); -- on attend fin envoi (8 bits + stop)
    wait for (clk_period * 8); -- le RxUnit travaille avec tmpclk décalée de 8 clocks
    wait for (clk_period); -- on attend que DRdy soit mis à jour
    wait for (clk_period / 2); -- on se désynchronise de l'horloge du RxUnit

    -- tester que la fin d'envoi est correcte
    assert DRdy = '1' report "donnée pas prête à être lue à temps" severity error;
    assert Ferr = '0' report "la donnée n'a pas été correctement reçue" severity error;
    assert data = "11110000" report "c'est pas la donnée qu'on a envoyée !!" severity error;
    read <= '1'; -- on prévient qu'on a lu
    wait for (clk_period); -- on attend que read soit vu
    assert OErr = '0' report "OErr alors que le processeur a lu la data" severity error;
    read <= '0';

    wait for (clk_period / 2); -- on se resynchronise de l'horloge du RxUnit
    wait for 100 ns;

    -- TEST TRANSMISSION ERREUR : BIT STOP MANQUANT
    data_in <= "10110111"; -- valeur aléatoire
    ld <= '1';
    wait for (clk_period); -- on attend que ld soit vu
    ld <= '0';
    wait for (clk_period * 10); -- on attend le vrai début de l'envoi
    wait for (clk_period * 16 * 9); -- on attend bit stop (start + 8 bits data)
    selection <= '0'; -- on force rxd à 0 pour causer une erreur de bit de stop
    wait for (clk_period * 9); -- le RxUnit travaille avec tmpclk décalée de 8 clocks
    wait for (clk_period / 2); -- on se resynchronise de l'horloge du RxUnit
    assert Ferr = '1' report "bit de stop manquant non détecté" severity error;
    selection <= '1';

    wait for (clk_period / 2); -- on se resynchronise de l'horloge du RxUnit
    wait for 100 ns;
    assert Ferr = '0' report "Ferr non réinitialisé" severity error;

    -- TEST TRANSMISSION ERREUR : DATA PAS LUE À TEMPS
    data_in <= "10110111"; -- valeur aléatoire
    ld <= '1';
    wait for (clk_period); -- on attend que ld soit vu
    ld <= '0';
    wait for (clk_period * 10); -- on attend le vrai début du second envoi (sans pause)
    wait for (clk_period * 16 * 9); -- on attend fin envoi (8 bits + stop)
    wait for (clk_period * 8); -- le RxUnit travaille avec tmpclk décalée de 8 clocks
    wait for (clk_period); -- on attend que DRdy soit mis à jour
    wait for (clk_period / 2); -- on se désynchronise de l'horloge du RxUnit
    wait for (clk_period * 2); -- on attend 2 tours de clock pour lire la data trop tard
    assert Oerr = '1' report "non détection que la donnée n'a pas été lue" severity error;

    wait for 100 ns;
    assert Oerr = '0' report "Oerr non réinitialisé" severity error;

    wait;

  end process;

END;
