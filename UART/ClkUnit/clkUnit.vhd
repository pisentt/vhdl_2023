LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY clkUnit IS

  PORT (
    clk, reset : IN STD_LOGIC;
    enableTX : OUT STD_LOGIC;
    enableRX : OUT STD_LOGIC);

END clkUnit;

ARCHITECTURE behavioral OF clkUnit IS

  -- horloge 1 Hz
  -- plateaux de durée égale
  CONSTANT facteur : NATURAL := 16;
  SIGNAL top : STD_LOGIC := '0';

BEGIN
  enableRX <= clk;

  div : PROCESS (clk, reset)
    VARIABLE cpt : INTEGER RANGE 0 TO facteur - 1 := 0;
  BEGIN
    IF reset = '0' THEN
      enableTX <= '0';
      cpt := 0;
    ELSIF rising_edge(clk) THEN
      IF (cpt = facteur - 1) THEN
        cpt := 0;
      ELSE
        cpt := cpt + 1;
      END IF;
      IF cpt < facteur - 1 THEN
        enableTX <= '0';
      ELSE
        enableTX <= '1';
      END IF;
    END IF;
  END PROCESS;

END behavioral;
