library IEEE;
use IEEE.std_logic_1164.all;

entity TxUnit is
  port (
  clk, reset : in std_logic;
  enable     : in std_logic;
  ld         : in std_logic;
  txd        : out std_logic;
  regE       : out std_logic;
  bufE       : out std_logic;
  data       : in std_logic_vector(7 downto 0));
end TxUnit;

architecture behavorial of TxUnit is

  -- recopie de bufE pour pouvoir
  -- lire et écrire la valeur dans le process
  signal bufE_sig : std_logic;


begin

  bufE <= bufE_sig;

  automate_envoi: process(reset, clk)
    -- liste des états :
    -- 000 -> idle
    -- 001 -> init_reg
    -- 010 -> init_envoi + bit de start
    -- 011 -> envoi_bit
    -- 100 -> fin_transmission + bit de stop
    variable etats : std_logic_vector(2 downto 0);

    -- tampon pour recevoir la donnée suivante
    variable buffer_T: std_logic_vector(7 downto 0);

    -- sauvegarder la donnée qu’on est en train d’envoyer
    variable register_T : std_logic_vector(7 downto 0);

    -- le compteur pour savoir quel bit envoyer
    variable cpt : natural;
  begin
    if reset = '0' then
      txd <= '1';
      bufE_sig <= '1';
      etats := "000";
    elsif rising_edge(clk) then
      case etats is
        when "000" =>
          regE <= '1';
          if ld = '1' then
            etats := "001";
          end if;
        when "001" =>
          register_T := buffer_T;
          regE <= '0';
          bufE_sig <= '1';
          etats := "010";
        when "010" =>
          if enable = '1' then
            txd <= '0';
            cpt := 7;
            etats := "011";
          end if;
        when "011" =>
          if enable = '1' and cpt > 0 then
            txd <= register_T(cpt);
            cpt := cpt - 1;
          elsif enable = '1' and cpt = 0 then
            txd <= register_T(0);
            etats := "100";
          end if;
        when "100" =>
          if enable = '1' then
            txd <= '1';
            if bufE_sig = '0' then
            -- on a déjà une donnée en cours,
            -- on relance la nouvelle transmission rapidement
              etats := "001";
            elsif bufE_sig = '1' then
              etats := "000";
            end if;
          end if;
        when others =>
          report "�tat anormal d�tect�";
      end case;

      if ld = '1' then
        buffer_T := data;
        bufE_sig <= '0';
      end if;
    end if;
  end process automate_envoi;

end behavorial;
