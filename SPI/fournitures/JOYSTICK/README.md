# Nexys4Joystick

## Utilisation
1. Flasher le fichier "Nexys4Joystick.bit" en utilisant le script dans `../Binaires`
2. Brancher le joystick sur les pins du bas du PMOD "JB" de la Nexys4 (non DDR)

## Fonctionnalités

- Le switch le plus à droite sert de "reset", mettre en position ON
- Le 2e switch le plus à droite sert de "enable", mettre en position ON
- Le 7segment affiche (dans l'ordre de gauche à droite) :
    1. Boutons appuyés sur le joystick
    2. Coordonnée y du joystick (3 digits)
    3. La constante '0' (caractère séparateur)
    4. Coordonnée x du joystick (3 digits)
- Appuyer sur un bouton du joystick allume la led correspondante sur le joystick
- La position du joystick est visible sur l'écran VGA
- Appuyer sur le click joystick change la couleur du cercle en jaune sur l'écran VGA
