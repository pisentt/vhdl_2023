library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Afficher une balle en VGA
entity balle is
  generic(rayon : natural);
  port ( centre_x : in std_logic_vector(9 downto 0); -- centre x de la balle
         centre_y : in std_logic_vector(9 downto 0); -- centre y de la balle
         pxl_x : in std_logic_vector(9 downto 0); -- coordonnee x du prochain pixel à dessiner
         pxl_y : in std_logic_vector(9 downto 0); -- coordonnee y du prochain pixel à dessiner
         in_cercle : out std_logic -- est-ce que le pixel est dans le cercle
       );
end balle;

architecture behavorial of balle is
  signal pos_x_sig : signed (10 downto 0) := (others => '0');
  signal pos_y_sig : signed (10 downto 0) := (others => '0');
begin

  pos_x_sig <= signed('0' & centre_x);
  pos_y_sig <= signed('0' & centre_y);

  -- process pour savoir si le pixel est dans le cercle
  process(pos_x_sig, pos_y_sig, pxl_x, pxl_y)
    variable dist_x : signed(10 downto 0);
    variable dist_y : signed(10 downto 0);
    variable dist : unsigned(21 downto 0);
  begin
    dist_x := signed('0' & pxl_x) - pos_x_sig;
    dist_y := signed('0' & pxl_y) - pos_y_sig;
    dist := unsigned(dist_x * dist_x) + unsigned(dist_y * dist_y);
    if dist < (rayon * rayon) then
      in_cercle <= '1';
    else
      in_cercle <= '0';
    end if;
  end process;
end behavorial;
