library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity MasterJoystick is
  port ( rst : in std_logic; -- reset du composant
         clk : in std_logic; -- horloge
         en : in std_logic;  -- débuter une transmission de 3 octets
         miso : in std_logic;  -- reception des bits envoyés par l’esclave
         joy_leds : in std_logic_vector(1 downto 0); -- allumer les leds du joystick
         ss   : out std_logic; -- slave-select
         sclk : out std_logic; -- horloge pour la transmission
         mosi : out std_logic; -- envoi des bits vers l’esclave
         joy_x : out std_logic_vector (9 downto 0); -- coordonnée x du joystick (transmission précédente)
         joy_y : out std_logic_vector (9 downto 0); -- coordonnée x du joystick (transmission précédente)
         btns : out std_logic_vector (2 downto 0);  -- états des boutons du joystick (transmission précédente)
         busy : out std_logic); -- transmission en cours
end MasterJoystick;

architecture behavior of MasterJoystick is

  -- Implémentation d’un maitre SPI qui envoie et reçoit un er_1octet
  -- dès qu’on met enable à 1
  component er_1octet is
    port ( rst : in std_logic ; -- reset du composant
           clk : in std_logic ; -- l’horloge du composant
           en : in std_logic ;  -- démarrer une transmission
           din : in std_logic_vector (7 downto 0) ; -- la donnée à envoyer lors de la prochaine transmission
           miso : in std_logic ;  -- réception des données de la part de l’esclave
           sclk : out std_logic ; -- l’horloge qu’on envoie à l’esclave
           mosi : out std_logic ; -- envoi des données à destination de l’esclave
           dout : out std_logic_vector (7 downto 0) ; -- octet reçu à la fin de la transmission
           busy : out std_logic); -- est-ce qu’on est en train de faire une transmission
  end component;

  signal cpt : std_logic_vector(2 downto 0); -- décompteur de 4 à 0 du nombre d’octets de la transmission en cours
  signal er_1octet_en, er_1octet_busy : std_logic; -- signaux de controle du er_1octet
  signal er_1octet_din, er_1octet_dout : std_logic_vector(7 downto 0); -- entrées-sorties du er_1octet

  constant t1 : integer := 30; -- temps attente entre ss et première transmission > 15 mini
  constant t2 : integer := 20; -- temps attente entre 2 transmissions > 10 mini
begin

  inst_er_1octet : er_1octet
  port map ( rst  => rst,
             clk  => clk,
             en   => er_1octet_en,
             din  => er_1octet_din,
             miso => miso,
             sclk => sclk,
             mosi => mosi,
             dout => er_1octet_dout,
             busy => er_1octet_busy
           );

  -- modification de l’entrée du er_1octet en fonction de l’indice de l’octet qu’on envoie
  -- exemple : er_1octet_din <= v1 when cpt = "10" else v2 when cpt = "01" else (others => '0');
  er_1octet_din <= "100000" & joy_leds when cpt = "100" else (others => '0'); -- on laisse toutes les leds éteintes

  -- automate à 3 états : idle, attente début transmission, transmission en cours
  controle_er_1octet : process(clk, rst)
    -- idle = "00", attente_transmission_commencée = "01", envoi_1octet = "10"
    variable etats : std_logic_vector(1 downto 0);

    -- compteur attente avant première transmission ou entre transmission
    variable t_cpt : unsigned(7 downto 0);
  begin
    if rst = '0' then
      etats := "00";
      busy <= '0';
      cpt <= "100";
      ss <= '1';
      t_cpt := to_unsigned(t1, 8); -- initialiser l’attente avant première transmission
    elsif rising_edge(clk) then
      if etats = "00" and en = '1' then
        busy <= '1';
        ss <= '0';
        if t_cpt > 0 then
          t_cpt := t_cpt - 1;
        else
          etats := "01";
          er_1octet_en <= '1';
        end if;
      elsif etats = "01" and er_1octet_busy = '1' then
        -- sécurité pour éviter de sortir prématuremment
        -- de l'attente de la fin de transmission
        etats := "10";
        er_1octet_en <= '0';
        t_cpt := to_unsigned(t2, 8); -- initialiser l’attente entre transmissions
      elsif etats = "10" and er_1octet_busy = '0' then
      -- quand on termine une transmission d'un octet

        -- envoyer la sortie du er_1octet dans la bonne sortie du MasterJoystick
        case cpt is
          when "100" =>
            joy_x(7 downto 0) <= er_1octet_dout;
          when "011" =>
            joy_x(9 downto 8) <= er_1octet_dout(1 downto 0);
          when "010" =>
            joy_y(7 downto 0) <= er_1octet_dout;
          when "001" =>
            joy_y(9 downto 8) <= er_1octet_dout(1 downto 0);
          when others =>
            btns <= er_1octet_dout(2 downto 0);
        end case;

        -- arret ou passer au prochain byte
        if cpt = "000" then
          ss <= '1';
          cpt <= "100";
          t_cpt := to_unsigned(t1, 8); -- initialiser l’attente avant première transmission
          etats := "00";
          busy <= '0';
        else
          if t_cpt > 0 then
            t_cpt := t_cpt - 1;
          else
            etats := "01";
            er_1octet_en <= '1';
            cpt <= std_logic_vector(unsigned(cpt) - 1);
          end if;
        end if;
      end if;
    end if;
  end process controle_er_1octet ;

end behavior;
