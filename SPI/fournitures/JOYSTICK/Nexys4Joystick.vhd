library IEEE;
use IEEE.std_logic_1164.all;
-- use IEEE.std_logic_arith.all;
-- use IEEE.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity Nexys4Joystick is
  port (
    -- les 16 switchs
    -- swt : in std_logic_vector (15 downto 0);
    rst_swt : in std_logic;
    en_swt : in std_logic;
    -- les 5 boutons noirs
    -- btnC, btnU, btnL, btnR, btnD : in std_logic;
    -- horloge
    mclk : in std_logic;
    -- les 16 leds
    -- led : out std_logic_vector (15 downto 0);
    -- les anodes pour sélectionner les afficheurs 7 segments à utiliser
    an : out std_logic_vector (7 downto 0);
    -- valeur affichée sur les 7 segments (point décimal compris, segment 7)
    ssg : out std_logic_vector (7 downto 0);

    jb_miso : in std_logic;  -- reception des bits envoyés par l’esclave
    jb_ss   : out std_logic; -- slave-select
    jb_sclk : out std_logic; -- horloge pour la transmission
    jb_mosi : out std_logic; -- envoi des bits vers l’esclave

    -- colors
    red   : out std_logic_vector(3 downto 0);
    green : out std_logic_vector(3 downto 0);
    blue  : out std_logic_vector(3 downto 0);
    -- signaux de synchro vga
    hsync : out std_logic;
    vsync : out std_logic
  );
end Nexys4Joystick;

architecture synthesis of Nexys4Joystick is

  -- rappel du (des) composant(s)
  component diviseurClk is
    -- facteur : ratio entre la fréquence de l'horloge origine et celle
    --           de l'horloge générée
    --  ex : 100 MHz -> 1Hz : 100 000 000
    --  ex : 100 MHz -> 1kHz : 100 000
    generic(facteur : natural);
    port (
    clk, reset : in  std_logic;
    nclk       : out std_logic);
  end component;


  component All7Segments is
    Port ( clk : in  std_logic;
           reset : in std_logic;
           e0 : in std_logic_vector (3 downto 0);
           e1 : in std_logic_vector (3 downto 0);
           e2 : in std_logic_vector (3 downto 0);
           e3 : in std_logic_vector (3 downto 0);
           e4 : in std_logic_vector (3 downto 0);
           e5 : in std_logic_vector (3 downto 0);
           e6 : in std_logic_vector (3 downto 0);
           e7 : in std_logic_vector (3 downto 0);
           an : out std_logic_vector (7 downto 0);
           ssg : out std_logic_vector (7 downto 0));
  end component;

  component MasterJoystick is
    port ( rst : in std_logic; -- reset du composant
           clk : in std_logic; -- horloge
           en : in std_logic;  -- débuter une transmission de 3 octets
           miso : in std_logic;  -- reception des bits envoyés par l’esclave
           joy_leds : in std_logic_vector(1 downto 0); -- allumer les leds du joystick
           ss   : out std_logic; -- slave-select
           sclk : out std_logic; -- horloge pour la transmission
           mosi : out std_logic; -- envoi des bits vers l’esclave
           joy_x : out std_logic_vector (9 downto 0); -- coordonnée x du joystick (transmission précédente)
           joy_y : out std_logic_vector (9 downto 0); -- coordonnée x du joystick (transmission précédente)
           btns : out std_logic_vector (2 downto 0);  -- états des boutons du joystick (transmission précédente)
           busy : out std_logic); -- transmission en cours
  end component;

  -- Afficher un cercle en VGA aux coordonnées du joystick
  component balle is
    generic(rayon : natural);
    port ( centre_x : in std_logic_vector(9 downto 0); -- centre x de la balle
           centre_y : in std_logic_vector(9 downto 0); -- centre y de la balle
           pxl_x : in std_logic_vector(9 downto 0); -- coordonnee x du prochain pixel à dessiner
           pxl_y : in std_logic_vector(9 downto 0); -- coordonnee y du prochain pixel à dessiner
           in_cercle : out std_logic -- est-ce que le pixel est dans le cercle
         );
  end component;

  -- controller les signaux de synchronisation du port VGA
  -- pour une image de 640 x 480 pixels à 60Hz
  component vga_controller
    port ( clk : in  std_logic; -- horloge du VGA de 25MHz
           rst : in  std_logic; -- reset
           hsync : out std_logic; -- synchro horizontale (fin de ligne)
           vsync : out std_logic; -- syncho verticale (fin d'image)
           next_x : out std_logic_vector(9 downto 0); -- prochain pixel à afficher : attention,
           next_y : out std_logic_vector(9 downto 0)  -- certains pixels ne sont pas dans la zone visible
         );
  end component;

  signal clk_spi, sig_busy : std_logic;
  signal sig_x, sig_y : std_logic_vector(9 downto 0);
  signal sig_btns : std_logic_vector(2 downto 0);

  signal saved_x, saved_y : std_logic_vector(11 downto 0);
  signal saved_btns : std_logic_vector(3 downto 0);

  signal clk_vga : std_logic;
  signal sig_in_cercle : std_logic;
  signal next_x, next_y, centre_x, centre_y : std_logic_vector(9 downto 0) := (others => '0');

  signal vsync_sig : std_logic;
begin

  div_clk_spi : diviseurClk
  generic map (100)
  port map ( clk => mclk,
             reset => rst_swt,
             nclk => clk_spi
           );

  inst_MasterJoystick : MasterJoystick
  port map ( rst => rst_swt,
             clk => clk_spi,
             en => en_swt,
             miso => jb_miso,
             joy_leds => saved_btns(2 downto 1),
             ss => jb_ss,
             sclk => jb_sclk, 
             mosi => jb_mosi,
             joy_x => sig_x,
             joy_y => sig_y,
             btns => sig_btns,
             busy => sig_busy
           );

  -- lors de l'affichage d'une nouvelle image, on enregistre les valeurs du joystick
  -- On actualise donc la valeur du joystick affichée à l'écran VGA et sur le 7segment 60 fois par seconde
  saved_x <= "00" & sig_x when rising_edge(vsync_sig);
  saved_y <= "00" & sig_y when rising_edge(vsync_sig);
  saved_btns <= "0" & sig_btns when rising_edge(vsync_sig);

  -- On affiche sur le 7 segment les valeurs enregistrées dans l'ordre (de gauche à droite)
  -- valeur boutons; coordonnee y (3 digits); "0" (symbole séparateur); coordonnee x (3 digits)
  inst_all7seg : All7Segments
  port map ( clk => mclk,
             reset => rst_swt,
             e7 => saved_btns,
             e6 => saved_x(11 downto 8),
             e5 => saved_x(7 downto 4),
             e4 => saved_x(3 downto 0),
             e3 => "0000",
             e2 => saved_y(11 downto 8),
             e1 => saved_y(7 downto 4),
             e0 => saved_y(3 downto 0),
             an => an,
             ssg => ssg
           );

  -- afficher un cercle aux coordonnées du joystick
  diviseurclk_vga : diviseurclk
  generic map(4) -- le vga a besoin d'une horloge à 25MHz
  port map(
            clk => mclk,
            reset => rst_swt,
            nclk => clk_vga
          );

  vga_controller_inst : vga_controller
  port map(
            clk => clk_vga,
            rst => rst_swt,
            hsync => hsync,
            vsync => vsync_sig,
            next_x => next_x,
            next_y => next_y
          );

  vsync <= vsync_sig;

  -- centre du cercle au centre de l'écran quand le joystick est en position neutre
  centre_x <= std_logic_vector(to_unsigned(149, 10) + (unsigned(saved_x(9 downto 0)) / 3));
  centre_y <= std_logic_vector(to_unsigned(411, 10) - (unsigned(saved_y(9 downto 0)) / 3));

  cercle : balle
  generic map(20) -- rayon de 20pixels pour le cercle
  port map ( centre_x => centre_x,
             centre_y => centre_y,
             pxl_x => next_x,
             pxl_y => next_y,
             in_cercle => sig_in_cercle
           );

  choisir_couleur : process(next_x, next_y, sig_in_cercle, saved_btns)
  begin
    -- IMPORTANT !! LES SIGNAUX RGB DOIVENT ETRE NULS EN DEHORS DE LA ZONE
    --              AFFICHABLE (initialisation de la masse)
    if (unsigned(next_x) < 640) and (unsigned(next_y) < 480) then
      if sig_in_cercle = '1' then -- si le pixel est dans la zone du cercle
        red <= (others => '1');

        -- balle en jaune si le bouton du joystick est enfoncé,
        -- rouge sinon
        if saved_btns(0) = '1' then
          green <= (others => '1');
        else
          green <= (others => '0');
        end if;
      else
        red <= (others => '0');
        green <= (others => '0');
      end if;

    else
      red <= (others => '0');
      green <= (others => '0');
      blue <= (others => '0');
    end if;
  end process;

end synthesis;
