library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Implémentation d’un maitre SPI qui envoie et reçoit un er_1octet
-- dès qu’on met enable à 1
entity er_1octet is
  port ( rst : in std_logic ; -- reset du composant
         clk : in std_logic ; -- l’horloge du composant
         en : in std_logic ;  -- démarrer une transmission
         din : in std_logic_vector (7 downto 0) ; -- la donnée à envoyer lors de la prochaine transmission
         miso : in std_logic ;  -- réception des données de la part de l’esclave
         sclk : out std_logic ; -- l’horloge qu’on envoie à l’esclave
         mosi : out std_logic ; -- envoi des données à destination de l’esclave
         dout : out std_logic_vector (7 downto 0) ; -- octet reçu à la fin de la transmission
         busy : out std_logic); -- est-ce qu’on est en train de faire une transmission
end er_1octet;

architecture behavioral_3 of er_1octet is
begin

  process(clk, rst)
    -- def des bits : 00 => idle; 01 => bit_emission; 10 => bit_reception
    variable etats : std_logic_vector(1 downto 0);
    variable registre : std_logic_vector(7 downto 0);
    variable cpt : natural;
  begin
    if rst = '0' then
      -- au reset on revient dans l'etat idle
      etats := "00";
      sclk <= '1';
      busy <= '0';
    elsif rising_edge(clk) then
      if (etats = "00") and (en = '1') then
        -- si on enable on quitte l’idle et on prépare la transmission
        registre := din;
        mosi <= registre(7);
        busy <= '1';
        sclk <= '0';
        cpt := 7; -- la transmission est en big endian
        etats := "01";
      elsif etats = "01" then
        -- sur les fronts montants de sclk on envoie un bit depuis le registre
        sclk <= '1';
        registre(cpt) := miso;
        etats := "10";
      elsif (etats = "10") and (cpt > 0) then
        -- sur les fronts descendant de sclk on reçoit un bit qu’on met dans le registre
        sclk <= '0';
        cpt := cpt - 1;
        mosi <= registre(cpt);
        etats := "01";
      elsif (etats = "10") and (cpt = 0) then
        -- transmission finie : on met la valeur du registre dans dout
        busy <= '0';
        dout <= registre;
        etats := "00"; -- on retourne en idle
      end if;
    end if;
  end process;

end behavioral_3;
