library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity MasterOpl is
  port ( rst : in std_logic; -- reset du composant
         clk : in std_logic; -- horloge
         en : in std_logic;  -- débuter une transmission de 3 octets
         v1 : in std_logic_vector (7 downto 0); -- première valeur à envoyer
         v2 : in std_logic_vector(7 downto 0);  -- deuxième valeur à envoyer
         miso : in std_logic;  -- reception des bits envoyés par l’esclave
         ss   : out std_logic; -- slave-select
         sclk : out std_logic; -- horloge pour la transmission
         mosi : out std_logic; -- envoi des bits vers l’esclave
         val_nand : out std_logic_vector (7 downto 0); -- résultat du nand de la transmission précédente
         val_nor : out std_logic_vector (7 downto 0);  -- résultat du nor de la transmission précédente
         val_xor : out std_logic_vector (7 downto 0);  -- résultat du xor de la transmission précédente
         busy : out std_logic); -- transmission en cours
end MasterOpl;

architecture behavior of MasterOpl is

  -- Implémentation d’un maitre SPI qui envoie et reçoit un er_1octet
  -- dès qu’on met enable à 1
  component er_1octet is
    port ( rst : in std_logic ; -- reset du composant
           clk : in std_logic ; -- l’horloge du composant
           en : in std_logic ;  -- démarrer une transmission
           din : in std_logic_vector (7 downto 0) ; -- la donnée à envoyer lors de la prochaine transmission
           miso : in std_logic ;  -- réception des données de la part de l’esclave
           sclk : out std_logic ; -- l’horloge qu’on envoie à l’esclave
           mosi : out std_logic ; -- envoi des données à destination de l’esclave
           dout : out std_logic_vector (7 downto 0) ; -- octet reçu à la fin de la transmission
           busy : out std_logic); -- est-ce qu’on est en train de faire une transmission
  end component;

  signal cpt : std_logic_vector(1 downto 0); -- décompteur de 2 à 0 du nombre d’octets de la transmission en cours
  signal er_1octet_en, er_1octet_busy : std_logic; -- signaux de controle du er_1octet
  signal er_1octet_din, er_1octet_dout : std_logic_vector(7 downto 0); -- entrées-sorties du er_1octet

  constant t1 : integer := 20; -- temps attente entre ss et première transmission
  constant t2 : integer := 10; -- temps attente entre 2 transmissions
begin

  inst_er_1octet : er_1octet
  port map ( rst  => rst,
             clk  => clk,
             en   => er_1octet_en,
             din  => er_1octet_din,
             miso => miso,
             sclk => sclk,
             mosi => mosi,
             dout => er_1octet_dout,
             busy => er_1octet_busy
           );

  -- modification de l’entrée du er_1octet en fonction de l’indice de l’octet qu’on envoie
  er_1octet_din <= v1 when cpt = "10" else v2 when cpt = "01" else (others => '0');

  -- automate à 3 états : idle, attente début transmission, transmission en cours
  controle_er_1octet : process(clk, rst)
    -- idle = "00", attente_transmission_commencée = "01", envoi_1octet = "10"
    variable etats : std_logic_vector(1 downto 0);

    -- compteur attente avant première transmission ou entre transmission
    variable t_cpt : unsigned(7 downto 0);
  begin
    if rst = '0' then
      etats := "00";
      busy <= '0';
      cpt <= "10";
      ss <= '1';
      t_cpt := to_unsigned(t1, 8); -- initialiser l’attente avant première transmission
    elsif rising_edge(clk) then
      if etats = "00" and en = '1' then
        busy <= '1';
        ss <= '0';
        if t_cpt > 0 then
          t_cpt := t_cpt - 1;
        else
          etats := "01";
          er_1octet_en <= '1';
        end if;
      elsif etats = "01" and er_1octet_busy = '1' then
        -- sécurité pour éviter de sortir prématuremment
        -- de l'attente de la fin de transmission
        etats := "10";
        er_1octet_en <= '0';
        t_cpt := to_unsigned(t2, 8); -- initialiser l’attente entre transmissions
      elsif etats = "10" and er_1octet_busy = '0' then
        -- quand on termine une transmission d'un octet

        -- envoyer la sortie du er_1octet dans la bonne sortie du MasterOpl
        case cpt is
          when "10" =>
            val_nand <= er_1octet_dout;
          when "01" =>
            val_nor  <= er_1octet_dout;
          when others =>
            val_xor  <= er_1octet_dout;
        end case;

        if cpt = "00" then
          ss <= '1';
          cpt <= "10";
          t_cpt := to_unsigned(t1, 8); -- initialiser l’attente avant première transmission
          etats := "00";
          busy <= '0';
        else
          if t_cpt > 0 then
            t_cpt := t_cpt - 1;
          else
            etats := "01";
            er_1octet_en <= '1';
            cpt <= std_logic_vector(unsigned(cpt) - 1);
          end if;
        end if;
      end if;
    end if;
  end process controle_er_1octet ;

end behavior;
