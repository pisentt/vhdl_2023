# MasterOpl

Je n'ai pas généré le output.vcd avec le script n7xilinxhelper mais avec mon propre Makefile et ghdl.
J'espère que cela vous ira tout de même, merci de me le faire savoir le cas échéant si le .vcd est incorrect.

## Regénerer le .vcd du MasterOpl

1. Lancer la commande : `make`
2. Si les tests passent pas, des erreurs vont apparaître dans la console (il y a des assertions dans le *test_MasterOpl_simple.vhd*)
3. Le fichier généré s'appelle *output1.vcd*
4. Ouvrir le fichier avec `gtkwave output1.vcd`

## Informations sur le test

Pour éviter des problèmes de synchronisation dans les assertions des tests, les valeurs des calculs du *MasterOpl* sont sauvegardées dans les signaux *saved_nand, saved_nor, saved_xor* à chaque fois que le signal *busy* retombe à 0.

Les tests réalisent 7 opérations différentes pour un total de 8 transmissions.
