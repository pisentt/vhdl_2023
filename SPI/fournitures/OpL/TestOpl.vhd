library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity testOpl is
  end testOpl;

architecture behavior of testOpl is

  component SlaveOpl is
    port(
          sclk : in  std_logic;
          mosi : in  std_logic;
          miso : out std_logic;
          ss   : in  std_logic
        );
  end component;

  component MasterOpl is
    port ( rst : in std_logic; -- reset du composant
           clk : in std_logic; -- horloge
           en : in std_logic;  -- débuter une transmission de 3 octets
           v1 : in std_logic_vector (7 downto 0); -- première valeur à envoyer
           v2 : in std_logic_vector(7 downto 0);  -- deuxième valeur à envoyer
           miso : in std_logic;  -- reception des bits envoyés par l’esclave
           ss   : out std_logic; -- slave-select
           sclk : out std_logic; -- horloge pour la transmission
           mosi : out std_logic; -- envoi des bits vers l’esclave
           val_nand : out std_logic_vector (7 downto 0); -- résultat du nand de la transmission précédente
           val_nor : out std_logic_vector (7 downto 0);  -- résultat du nor de la transmission précédente
           val_xor : out std_logic_vector (7 downto 0);  -- résultat du xor de la transmission précédente
           busy : out std_logic); -- transmission en cours
  end component;

  --Inputs
  signal clk : std_logic := '0';
  signal reset : std_logic := '0';
  signal en : std_logic := '0';
  signal val1 : std_logic_vector(7 downto 0);
  signal val2 : std_logic_vector(7 downto 0);

  --Outputs
  signal val_nand : std_logic_vector (7 downto 0);
  signal val_nor : std_logic_vector (7 downto 0);
  signal val_xor : std_logic_vector (7 downto 0);
  signal busy : std_logic;

  --Saved Outputs (derrière valeurs de val_nand etc…
  -- avant que le MasterOpl redevienne busy)
  signal saved_val_nand : std_logic_vector (7 downto 0);
  signal saved_val_nor : std_logic_vector (7 downto 0);
  signal saved_val_xor : std_logic_vector (7 downto 0);

  --References
  signal ss, sclk, miso, mosi : std_logic := '0';

  -- Clock period definitions
  constant clk_period : time := 10 ns;

begin

  reset <= '1' after 10 ns;

  inst_SlaveOpl : SlaveOpl
  port map (
             sclk => sclk,
             mosi => mosi,
             miso => miso,
             ss   => ss
           );

  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;

  inst_MasterOpl : MasterOpl
  port map ( rst  => reset,
             clk  => clk,
             en   => en,
             v1   => val1,
             v2   => val2,
             miso => miso,
             ss   => ss,
             sclk => sclk,
             mosi => mosi,
             val_nand => val_nand,
             val_nor  => val_nor,
             val_xor  => val_xor,
             busy     => busy
           );

  saving : process(clk)
  begin
    if rising_edge(clk) and busy = '0' then
      saved_val_nand <= val_nand;
      saved_val_nor <= val_nor;
      saved_val_xor <= val_xor;
    end if;
  end process;

  nouveau_calcul : process
  begin
    en <= '1';
    wait for 0.5 us;
    en <= '0';
    wait for 2.5 us;
  end process;

  val1 <= "00000000",
          "11111111" after 3 us,
          "11111111" after 6 us,
          "11110000" after 9 us,
          "11110000" after 12 us,
          "11011011" after 15 us,
          "11011011" after 18 us;

  val2 <= "00000000",
          "00000000" after 3 us,
          "11111111" after 6 us,
          "11110000" after 9 us,
          "00000000" after 12 us,
          "01010101" after 15 us,
          "11100111" after 18 us;

  verification : process
  begin
    wait for 3 us;
    assert(saved_val_nand = "00110011") report "nand_0 faux"
    severity error;
    assert(saved_val_nor  = "11001100") report "nor_0  faux"
    severity error;
    assert(saved_val_xor  = "10100000") report "xor_0  faux"
    severity error;

    wait for 3 us;
    assert(saved_val_nand = "11111111") report "nand_1 faux"
    severity error;
    assert(saved_val_nor  = "11111111") report "nor_1  faux"
    severity error;
    assert(saved_val_xor  = "00000000") report "xor_1  faux"
    severity error;

    wait for 3 us;
    assert(saved_val_nand = "11111111") report "nand_2 faux"
    severity error;
    assert(saved_val_nor  = "00000000") report "nor_2  faux"
    severity error;
    assert(saved_val_xor  = "11111111") report "xor_2  faux"
    severity error;

    wait for 3 us;
    assert(saved_val_nand = "00000000") report "nand_3 faux"
    severity error;
    assert(saved_val_nor  = "00000000") report "nor_3  faux"
    severity error;
    assert(saved_val_xor  = "00000000") report "xor_3  faux"
    severity error;

    wait for 3 us;
    assert(saved_val_nand = "00001111") report "nand_4 faux"
    severity error;
    assert(saved_val_nor  = "00001111") report "nor_4  faux"
    severity error;
    assert(saved_val_xor  = "00000000") report "xor_4  faux"
    severity error;

    wait for 3 us;
    assert(saved_val_nand = "11111111") report "nand_5 faux"
    severity error;
    assert(saved_val_nor  = "00001111") report "nor_5  faux"
    severity error;
    assert(saved_val_xor  = "11110000") report "xor_5  faux"
    severity error;

    wait for 3 us;
    assert(saved_val_nand = "10101110") report "nand_6 faux"
    severity error;
    assert(saved_val_nor  = "00100000") report "nor_6  faux"
    severity error;
    assert(saved_val_xor  = "10001110") report "xor_6  faux"
    severity error;

    wait for 3 us;
    assert(saved_val_nand = "00111100") report "nand_7 faux"
    severity error;
    assert(saved_val_nor  = "00000000") report "nor_7  faux"
    severity error;
    assert(saved_val_xor  = "00111100") report "xor_7  faux"
    severity error;

    wait for 100 us;
  end process;



end behavior;
