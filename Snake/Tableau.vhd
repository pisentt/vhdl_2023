library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Tableau is
  port( x : in std_logic_vector(5 downto 0);
        y : in std_logic_vector(4 downto 0);
        val : in std_logic;
        write : in std_logic;
        color : out std_logic
      );
end Tableau;

architecture behavior of Tableau is
  variable contenu : std_logic_vector(1199 downto 0)
begin
  process(x, y, write) is
    variable i : integer;
  begin
    i := to_unsigned(y) * 40 + to_unsigned(x);
    if rising_edge(write) then
      color <= contenu(to_integer(to_unsigned(x)));
    end if;
  end process;
end behavior;
