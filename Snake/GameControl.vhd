library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity GameControl is
  port(clk : in std_logic;
       direction : in std_logic_vector(1 downto 0); -- direction (haut, bas, gauche, droite)
       vsync : in std_logic; -- signal vsync du vga
       vga_x : in std_logic_vector(9 downto 0); -- coord x du vga
       vga_y : in std_logic_vector(9 downto 0); -- coord y du vga
       fifo_in : in std_logic_vector(15 downto 0); -- derniere valeur pop du fifo
       busy : in std_logic; -- busy du tableau et du fifo
       tab_x : out std_logic_vector(5 downto 0); -- coord x pour le tableau
       tab_y : out std_logic_vector(4 downto 0); -- coord y pour le tableau
       tab_val : out std_logic; -- valeur à mettre dans le tableau
       tab_write : out std_logic; -- sur front montant : écrire tab_val dans le tableau
       fifo_out : out std_logic_vector(15 downto 0); -- valeur à append dans le fifo
       fifo_append : out std_logic; -- sur front montant : append val_out dans le fifo
       fifo_pop : out std_logic -- sur front montant : pop la valeur la plus ancienne du fifo
      );
end GameControl;

architecture behavior of GameControl is
  signal etats : std_logic_vector(1 downto 0);
  -- 00 : afficher_ecran; 01 : update_coords;
  -- 10 : attente_append; 11 : attente_pop

  signal pos_x : std_logic_vector(7 downto 0); -- coords de la tete du snake
  signal pos_y : std_logic_vector(7 downto 0);
begin
  -- afficher le tableau pendant l'affichage du vga
  tab_x <= std_logic_vector(unsigned(vga_x) / 16) when etats = "00";
  tab_y <= std_logic_vector(unsigned(vga_y) / 16) when etats = "00";

  -- démarrer le cycle pendant le temps inter-frame

  -- update les coodonnées de la tête du serpent
  pos_y <= std_logic_vector(unsigned(pos_y) - 1) when rising_edge(vsync) and direction = "00";
  pos_y <= std_logic_vector(unsigned(pos_y) + 1) when rising_edge(vsync) and direction = "01";
  pos_x <= std_logic_vector(unsigned(pos_x) - 1) when rising_edge(vsync) and direction = "10";
  pos_x <= std_logic_vector(unsigned(pos_x) + 1) when rising_edge(vsync) and direction = "11";

  etats <= "01" when rising_edge(vsync);

  -- preparer le changement de couleur de la tete et l'append
  fifo_out <= pos_x & pos_y;

  tab_x <= pos_x(5 downto 0) when etats = "01";
  tab_y <= pos_y(4 downto 0) when etats = "01";

  -- append les nouvelles coords dans la fifo et changer couleur tete du serpent
  fifo_append <= '1' when rising_edge(clk) and etats = "01";
  tab_write   <= '1' when rising_edge(clk) and etats = "01";

  etats <= "10" when rising_edge(clk) and etats = "01";

  fifo_append <= '0' when rising_edge(clk) and etats = "10";
  tab_write   <= '0' when rising_edge(clk) and etats = "10";

  -- quand le append est fini on lance le pop
  etats   <= "11" when rising_edge(clk) and etats = "10" and busy = '0';
  fifo_pop <= '1' when rising_edge(clk) and etats = "10" and busy = '0';
end behavior;
