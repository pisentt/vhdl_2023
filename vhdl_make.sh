#!/usr/bin/env bash

if (($# != 1)); then
  echo "usage: vhdl_make.sh <chemin du dossier>"
  exit
fi

cat <<EOF> $1/Makefile
GHDL=ghdl
GHDLFLAGS=--std=08

TIME=30us
PLOT=output

SOURCES_FILES=\$(wildcard *.vhd)
VCD=\$(patsubst %.vhd, %.vcd, \$(filter test%, \$(SOURCES_FILES)))

all: \$(VCD)

# Binary depends on the object file
\$(VCD):%.vcd: %
	\$(GHDL) -r \$(GHDLFLAGS) \$* \\
		--stop-time=\${TIME} \\
		--vcd=\$@

# Object file depends on source
%.o: %.vhd
	\$(GHDL) -a \$(GHDLFLAGS) \$<

clean:
	echo "Cleaning up..."
	\$(GHDL) --remove \$(GHDLFLAGS)
	#	rm -f \${PLOT}.vcd
EOF


# pour tous les fichiers source
for s in $1/*.vhd; do
  obj=$(basename $s | sed "s/.vhd/.o/")
  # créer la règle pour une entité (en lui donnant le bon fichier source)
  entities=$(grep -i "entity *\w*\($\| *is\)" $s | sed "s/.*entity *\(\w*\).*/\1/I" | tr '\n' ' ')
  for entity in $entities; do
    echo -e "\n$entity: $obj" >> $1/Makefile
    echo -e "\t\$(GHDL) -e \$(GHDLFLAGS) \$@" >> $1/Makefile # necessaire pour la version gcc de vhdl
  done

  # créer la règle pour compiler un fichier source
  # (en lui donnant les entités dont il dépend)
  deps=$(grep -i "component *\w*\($\| *port\| *is\)" $s | sed "s/.*component *\(\w*\).*/\1/I")
  if [ -n "$deps" ]; then
    echo -e "\n$obj: $(echo $deps | tr '\n' ' ')" >> $1/Makefile
  fi
done
