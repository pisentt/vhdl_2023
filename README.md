# VHDL_2023

*Téo Pisenti*

Sources de mes projets VHDL en 2e année ASR à l'ENSEEIHT. Les projets sont réalisés pour une carte Nexys 4 (version originale et non la version DDR).
Des `Makefile` permettent de simuler les sources avec le logiciel libre [ghdl](https://github.com/ghdl/ghdl) et d'obtenir des fichier .vcd que l'on peut visualiser avec le logiciel libre [gtkwave](https://github.com/gtkwave/gtkwave).
Pour générer les binaires pour la Nexys4, vous devrez obligatoirement passer par le logiciel propriétaire Xilinx ou Vivado.
Des binaires (fichier ".bit") pour la Nexys4 sont disponibles dans ce repo git, vous pouvez les flasher sur la carte Nexys4 avec le logiciel libre [openocd](https://github.com/openocd-org/openocd).

## Liste des projets

- VgaHDL: projet personnel, implantation d'un controlleur VGA pour connecter la Nexys4 à un écran VGA
- SPI: implantation du protocole SPI en VHDL pour la Nexys4. Le dossier SPI/JOYSTICK contient une instantiation du controlleur SPI qui permet de controller un point sur un écran VGA à l'aide d'un joystick
- UART: implantation du protocole de liasion série UART en VHDL pour la Nexys4
- Snake: projet abandonné de faire un jeu de snake qui se piloterait avec le joystick du projet SPI/JOYSTICK et qui afficherait le jeu sur un écran VGA à l'aide du controlleur VgaHDL. Ce projet n'a pas été réellement commencé par manque de temps et parce que je ne possède plus de carte Nexys4. Je serais ravi de travailler dessus dans le futur si quelqu'un est interessé

## Simulation avec GHDL

Installez [GHDL](https://github.com/ghdl/ghdl) sur votre ordinateur. Remarque: La version en ADA convient très bien et est plus simple à installer que les autres.

Installez gnumake sur votre ordinateur pour avoir la commande `make` et gtkwave pour pouvoir visualiser les fichiers .vcd résultant des simulations.

Quand vous voyez un fichier `Makefile`, vous pouvez en général l'utiliser pour simuler les fichiers `.vhdl` avec les commandes suivante:

- `make nom_du_module_vhdl`: compiler le module vhdl demandé
- `make nom_du_module_vhdl.vcd`: compiler le module vhdl demandé si nécessaire, puis l'exécuter et écrire le résultat dans le fichier `nom_du_module_vhdl.vcd`. Vous pouvez le visualiser avec `gtkwave nom_du_module_vhdl.vcd`

### Générer un Makefile à partir de sources VHDL

Le script `vhdl_make.sh` permet de générer automatiquement un fichier Makefile à partir des sources VHDL d'un dossier. Il va automatiquement détecter les modules, y compris si on a plusieurs modules dans un même fichier et automatiquement écrire les dépendances entre les fichiers dans le Makefile. Vous obtiendrez un Makefile à utiliser avec VHDL comme expliqué dans la section précédente.

## Flasher un binaire .bit sur une carte Nexys4

Installez le logiciel libre [openocd](https://github.com/openocd-org/openocd) pour pouvoir flasher un fichier `.bit` généré avec le logiciel propriétaire Xilinx ou Vivado sur une carte Nexys4.

Flasher un fichier `.bit` avec le script: `./SPI/fournitures/Binaires/program.sh nom_du_fichier.bit`

Openocd prend en paramètre les fichiers de config "digilent-hs1.cfg" et "xilinx-xc7.cfg" qui sont dépendant du FPGA utilisé. Les scripts fournis devraient (en théorie) fonctionner pour toute les cartes basé sur la puce FPGA "Artix7" de Xilinx. En particulier pour la Nexys4 et Nexys4-DDR.
