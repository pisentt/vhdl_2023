#!/bin/bash

openocd -f digilent-hs1.cfg -f xilinx-xc7.cfg -c "transport select jtag;init; pld load 0 $1; exit"
