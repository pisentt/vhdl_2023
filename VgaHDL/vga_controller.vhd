library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- controlleur vga pour une image de 640 x 480 pixels à 60Hz

entity vga_controller is
    port ( clk : in  std_logic; -- horloge à 25MHz
           rst : in  std_logic; -- reset
           hsync : out std_logic; -- synchro horizontale (fin de ligne)
           vsync : out std_logic; -- synchro verticale (fin d'image)
           next_x : out std_logic_vector(9 downto 0); -- coordonnées du prochain pixel à traiter
           next_y : out std_logic_vector(9 downto 0)  -- ATTENTION : certains pixels sont pas visibles
         );
end vga_controller;

-- IMPORTANT !!
-- On utilise les coordonnées de pixels pour gérer les temps de synchronisation du protocole VGA
-- On a donc certaines coordonnées de pixels qui sont en dehors de la zone d'affichage et qui
-- ne doivent pas avoir de couleur.
-- Ils sont en sortie du composant uniquement pour que le programeur sache à quel étape du protocole
-- VGA on en est.
-- Les durées de chaque étape (front porch, back porch, synchro, affichage de pixels visibles, etc..)
-- sont données en pixels dans les commentaires qui suivent.

architecture behavioral of vga_controller is
begin

  process(clk, rst)

    variable x_aux : unsigned (9 downto 0);
    variable y_aux : unsigned (9 downto 0);

  begin

    if (rst = '0') then
      x_aux := (others => '0');
      y_aux := (others => '0');
    elsif(rising_edge(clk)) then

      -- generation de hsync dans l'ordre
      -- pixels visibles (640); front porch (16); hsync (96); back porch (48)
      if (x_aux > 655) and (x_aux < 752) then
        hsync <= '1';
      else
        hsync <= '0';
      end if;

      -- generation de vsync dans l'ordre
      -- pixels visibles (480); front porch (10); vsync (2); back porch (33)
      if (y_aux > 489) and (y_aux < 492) then
        vsync <= '1';
      else
        vsync <= '0';
      end if;

      -- on incremente nos pixels
      x_aux := x_aux + 1;
      if x_aux >= 800 then
        x_aux := (others => '0');
        y_aux := (y_aux + 1) rem 525;
      end if;

      next_x <= std_logic_vector(x_aux);
      next_y <= std_logic_vector(y_aux);
    end if;

  end process;

end behavioral;
