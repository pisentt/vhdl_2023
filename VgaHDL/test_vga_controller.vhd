library ieee;
use ieee.std_logic_1164.all;
 
entity test_vga_controller is
end test_vga_controller;
 
architecture behavior of test_vga_controller is 
   
  -- component declaration for the unit under test (uut)
  component vga_controller is
    port ( clk : in  std_logic;
           rst : in  std_logic;
           hsync : out std_logic;
           vsync : out std_logic;
           next_x : out std_logic_vector(9 downto 0);
           next_y : out std_logic_vector(9 downto 0)
         );
  end component;

   --inputs
  signal clk : std_logic := '0';
  signal rst : std_logic := '0';

   --outputs
  signal hsync : std_logic;
  signal vsync : std_logic;
  signal next_x : std_logic_vector(9 downto 0);
  signal next_y : std_logic_vector(9 downto 0);

  -- clock period definitions
  constant clk_period : time := 40 ns;

begin

   -- Instantiate the Unit Under Test (UUT)
  uut: vga_controller
  port map(
            clk => clk,
            rst => rst,
            hsync => hsync,
            vsync => vsync,
            next_x => next_x,
            next_y => next_y
          );


  -- clock process definitions
  clk_process :process
  begin
      clk <= '0';
      wait for clk_period/2;
      clk <= '1';
      wait for clk_period/2;
  end process;


  -- stimulus process
  stim_proc: process
  begin		
    -- hold reset state for 100 ns.
    wait for 100 ns;	
    rst <= '1';

    wait for clk_period;

    wait;
  end process;

end;
