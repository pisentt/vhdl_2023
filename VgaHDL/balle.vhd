library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity balle is
  generic(rayon : natural);
  port ( clk_position : in std_logic;
         rst : in std_logic;
         pxl_x : in std_logic_vector(9 downto 0);
         pxl_y : in std_logic_vector(9 downto 0);
         in_cercle : out std_logic;
         dist_debug : out std_logic_vector(21 downto 0)
       );
end balle;

architecture behavorial of balle is
  signal pos_x_sig : signed (10 downto 0);
  signal pos_y_sig : signed (10 downto 0);
begin
  process(clk_position, rst)
    variable pos_x : unsigned (9 downto 0);
    variable pos_y : unsigned (9 downto 0);
    variable dx : std_logic; -- 1 pour additionner, 0 pour soustraire
    variable dy : std_logic;
  begin
    if rst = '0' then
      pos_x := to_unsigned(rayon, 10) + 1;
      pos_y := to_unsigned(rayon, 10) + 1;
      dx := '1';
      dy := '1';
    elsif rising_edge(clk_position) then
      if (pos_x <= rayon) or (pos_x + rayon >= 640) then
        dx := not dx;
      end if;
      if (pos_y <= rayon) or (pos_y + rayon >= 480) then
        dy := not dy;
      end if;
      if dx = '1' then
        pos_x := pos_x + 1;
      else
        pos_x := pos_x - 1;
      end if;
      if dy = '1' then
        pos_y := pos_y + 1;
      else
        pos_y := pos_y - 1;
      end if;
    end if;
    pos_x_sig <= signed('0' & pos_x);
    pos_y_sig <= signed('0' & pos_y);
  end process;


  process(pos_x_sig, pos_y_sig, pxl_x, pxl_y)
    variable dist_x : signed(10 downto 0);
    variable dist_y : signed(10 downto 0);
    variable dist : unsigned(21 downto 0);
  begin
    dist_x := signed('0' & pxl_x) - pos_x_sig;
    dist_y := signed('0' & pxl_y) - pos_y_sig;
    dist := unsigned(dist_x * dist_x) + unsigned(dist_y * dist_y);
    dist_debug <= std_logic_vector(dist);
    if dist < (rayon * rayon) then
      in_cercle <= '1';
    else
      in_cercle <= '0';
    end if;
  end process;
end behavorial;
