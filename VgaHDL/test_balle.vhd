library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity test_balle is
end test_balle;
 
architecture behavior of test_balle is 
   
component balle is
  generic(rayon : natural);
  port ( clk_position : in std_logic;
         rst : in std_logic;
         pxl_x : in std_logic_vector(9 downto 0);
         pxl_y : in std_logic_vector(9 downto 0);
         in_cercle : out std_logic;
         dist_debug : out std_logic_vector(21 downto 0)
       );
end component;



   --inputs
  signal clk_position : std_logic := '0';
  signal rst : std_logic := '0';

   --outputs
  signal pxl_x : std_logic_vector(9 downto 0);
  signal pxl_y : std_logic_vector(9 downto 0);
  signal in_cercle : std_logic;
  signal dist_debug : std_logic_vector(21 downto 0);

  -- clock period definitions
  constant clk_period : time := 100 us;
  signal clk : std_logic;

begin

   -- Instantiate the Unit Under Test (UUT)
  uut: balle
  generic map(4)
  port map(
            clk_position => clk_position,
            rst => rst,
            pxl_x => pxl_x,
            pxl_y => pxl_y,
            in_cercle => in_cercle,
            dist_debug => dist_debug
          );


  -- clock process definitions
  proc_clk : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;

  process(clk, rst)
    variable x : unsigned (9 downto 0);
    variable y : unsigned (9 downto 0);
  begin
    if rst = '0' then
      x := (others => '0');
      y := (others => '0');
    elsif (rising_edge(clk)) then
      if x < 10 then
        x := x + 1;
      else
        x := (others => '0');
        y := y + 1;
      end if;
    end if;
    pxl_x <= std_logic_vector(x);
    pxl_y <= std_logic_vector(y);
  end process;


  -- stimulus process
  stim_proc: process
  begin		
    -- hold reset state for 100 ns.
    wait for 10 ns;	
    rst <= '1';
    wait for 10 ns;	
    clk_position <= '1',
                    '0' after 4 ns,
                    '1' after 6 ns,
                    '0' after 8 ns,
                    '1' after 9 ns,
                    '0' after 10 ns,
                    '1' after 12 ns,
                    '0' after 14 ns;
                    -- '1' after 16 ns,
                    -- '0' after 18 ns,
                    -- '1' after 20 ns,
                    -- '0' after 22 ns;


    wait for clk_period;

    wait;
  end process;

end;
