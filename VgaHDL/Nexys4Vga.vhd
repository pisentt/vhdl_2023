library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity Nexys4Vga is
  port (
         -- horloge interne
         mclk : in std_logic;
         -- switches
         rst_swt : in std_logic;
         rst_balle_swt : in std_logic;
         -- colors
         red   : out std_logic_vector(3 downto 0);
         green : out std_logic_vector(3 downto 0);
         blue  : out std_logic_vector(3 downto 0);
         -- signaux de synchro vga
         hsync : out std_logic;
         vsync : out std_logic
       );
end Nexys4Vga;

architecture synthesis of Nexys4Vga is

  component diviseurclk
    generic(facteur: natural);
    port(
          clk : in  std_logic;
          reset : in  std_logic;
          nclk : out  std_logic
        );
  end component;

  component vga_controller
    port ( clk : in  std_logic;
           rst : in  std_logic;
           hsync : out std_logic;
           vsync : out std_logic;
           next_x : out std_logic_vector(9 downto 0);
           next_y : out std_logic_vector(9 downto 0)
         );
  end component;

  component balle is
    generic(rayon : natural);
    port ( clk_position : in std_logic;
           rst : in std_logic;
           pxl_x : in std_logic_vector(9 downto 0);
           pxl_y : in std_logic_vector(9 downto 0);
           in_cercle : out std_logic;
           dist_debug : out std_logic_vector(21 downto 0)
         );
  end component;


  signal nclk : std_logic;
  signal next_x, next_y : std_logic_vector(9 downto 0);
  signal vsync_aux : std_logic;

  -- tester si on est dans le cercle de la balle
  signal in_cercle : std_logic;
begin

  diviseurclk_inst : diviseurclk
  generic map(4)
  port map(
            clk => mclk,
            reset => rst_swt,
            nclk => nclk
          );

  vga_controller_inst : vga_controller
  port map(
            clk => nclk,
            rst => rst_swt,
            hsync => hsync,
            vsync => vsync_aux,
            next_x => next_x,
            next_y => next_y
          );

  vsync <= vsync_aux;

  -- une petite balle qui se deplace a l'ecran
  balle_inst : balle
  generic map(20)
  port map(
            clk_position => vsync_aux,
            rst => rst_balle_swt,
            pxl_x => next_x,
            pxl_y => next_y,
            in_cercle => in_cercle,
            dist_debug => open
          );

  process(next_x, next_y, in_cercle)
  begin
    if (unsigned(next_x) < 640) and (unsigned(next_y) < 480) then
      red <= (others => '1');

      if in_cercle = '1' then
        blue <= (others => '0');
		  green <= (others => '0');
      else
        blue <= (others => '1');
		  green <= (others => '1');
      end if;
    else
      red <= (others => '0');
      green <= (others => '0');
      blue <= (others => '0');
    end if;
  end process;


end architecture synthesis;
